#ifndef ABSTRACTSEEDSANALYZER_H
#define	ABSTRACTSEEDSANALYZER_H

#include <vector>

using namespace std;

class AbstractSeedsAnalyzer {
public:
    AbstractSeedsAnalyzer();
    AbstractSeedsAnalyzer(const AbstractSeedsAnalyzer& orig);
    virtual ~AbstractSeedsAnalyzer();
    virtual double getProbability(unsigned regionLenght = 64, double p = 0.7) = 0;
    virtual void setSeeds(vector<vector<bool>> &seedSet) = 0;
    virtual void readAndCompute() = 0;
private:

};

#endif	/* ABSTRACTSEEDSANALYZER_H */

