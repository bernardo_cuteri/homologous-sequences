#include "BestSetSeeeker.h"
#include "DPAlgorithm.h"
#include <algorithm>
#include <climits>

BestSetSeeeker::BestSetSeeeker(int l, int w, int n, int r, double p, int bF, int cPL, int mBD) : l(l), w(w), nSeeds(n), r(r), p(p), bestP(0), branchingFactor(bF), candidatesPerLength(cPL), maxBranchingDepth(mBD) {

}

BestSetSeeeker::~BestSetSeeeker() {
}

void initialize(vector<bool> & seed, int li, int w) {
    seed[0] = seed[li - 1] = 1;
    for (int i = 0; i < w - 2; i++) {
        seed[li - i - 2] = 1;
    }
}

bool moreOnesOnTheRightOrEqual(vector<bool> & seed) {
    int cont = 0;
    int half = seed.size() / 2;
    for (int i = 0; i < half; i++) {
        if (seed[i]) {
            cont++;
        }
    }
    if (seed.size() % 2) {
        half++;
    }
    for (int i = half; i < seed.size(); i++) {
        if (seed[i]) {
            cont--;
        }
    }
    return cont <= 0;

}

void BestSetSeeeker::possiblyInsertSeed(vector<bool> & seed, double pMeasure, map<double, vector < bool>> &bestSeedsForS, bool highestBest, int bf) {
    if (highestBest && bestSeedsForS.size() > bf - 1 && pMeasure <= bestSeedsForS.begin()->first) {
        return;
    }
    if (!highestBest && bestSeedsForS.size() > bf - 1 && pMeasure >= bestSeedsForS.rbegin()->first) {
        return;
    }
    bestSeedsForS[pMeasure] = seed;
    if (bestSeedsForS.size() > bf) {
        if (highestBest) {
            bestSeedsForS.erase(bestSeedsForS.begin());
        } else {
            bestSeedsForS.erase(prev(bestSeedsForS.end()));
        }
    }
}

void BestSetSeeeker::possiblyUpdateBestSeedSet(vector<vector<bool> >& s, double pHit) {
    if (pHit > bestP) {
        bestSeedSet = s;
        bestP = pHit;
        cout << "improving p:" << bestP << "\n";
        SharedFunctions::printSeedSet(bestSeedSet);
        cout << "after ";
        SharedFunctions::printElapsedTime(bestSeedComputationStart);
        cout << "\n\n";
    }
}

int BestSetSeeeker::getBestLengthIndex(map<double, vector<bool>>*bestSeedsForS, DPAlgorithm & sAnalyzer, vector<vector<bool> >& s, bool completeSet, double & pHit) {
    double bestPM = 0;
    int bestLengthIndex;
    for (int i = l - w; i >= 0; i--) {
        if (!(bestSeedsForS[i].empty())) {
            s.push_back(bestSeedsForS[i].begin()->second);
            sAnalyzer.addSeed(bestSeedsForS[i].begin()->second);
            double pHit = sAnalyzer.getProbability(r, p);
            if (pHit > bestPM) {
                bestLengthIndex = i;
                bestPM = pHit;
            }
            if (completeSet) {
                possiblyUpdateBestSeedSet(s, pHit);
            }
            s.pop_back();
            sAnalyzer.pop_back(true);
        }
    }
    pHit = bestPM;
    return bestLengthIndex;
}

void BestSetSeeeker::recursiveFindBestSeedSetWithOCHeuristic(vector<vector<bool> >& s, DPAlgorithm & sAnalyzer, int depth, unsigned sOC) {
    cout << "depth: " << depth << "\n";
    Clock::time_point t0 = Clock::now();
    map<double, vector<bool>>*bestSeedsForS = new map<double, vector<bool>>[l - w + 1];
    int decreasingBranching = branchingFactor;
    if (depth > maxBranchingDepth) {
        decreasingBranching = 1;
    }
    int adjustedCandidatesPerLength = max(candidatesPerLength, decreasingBranching);
    //for all possible seeds
    for (int li = l; li > w; li--) {
        vector<bool> seed(li, 0);
        initialize(seed, li, w);
        do {
            if (depth > 0 || moreOnesOnTheRightOrEqual(seed)) {
                //avoid to compute p on a seed and the symmetric one (we know it's the same)
                //s.push_back(seed);
                unsigned pMeasure = sAnalyzer.getIncrementalOverlapComplexity(sOC, seed);
                possiblyInsertSeed(seed, pMeasure, bestSeedsForS[li - w], false, adjustedCandidatesPerLength);
                //s.pop_back();
            }
        } while (next_permutation(seed.begin() + 1, seed.end() - 1));
    }
    //cout<<"permutations done\n";
    //SharedFunctions::printElapsedTime(t0);
    t0 = Clock::now();
    if (depth == nSeeds - 1) {
        //first we figure out the best length
        double bestLengthFirstPHit;
        int bestLengthIndex = getBestLengthIndex(bestSeedsForS, sAnalyzer, s, true, bestLengthFirstPHit);
        map<double, vector<bool> >::iterator it = bestSeedsForS[bestLengthIndex].begin();
        //we already checked the first
        it++;
        for (; it != bestSeedsForS[bestLengthIndex].end(); it++) {
            s.push_back(it->second);
            sAnalyzer.addSeed(it->second);
            double pHit = sAnalyzer.getProbability(r, p);
            possiblyUpdateBestSeedSet(s, pHit);
            s.pop_back();
            sAnalyzer.pop_back(true);
        }
        //cout<<"leaf reached\n";
        //SharedFunctions::printElapsedTime(t0);
    } else {
        //select the best branchingFactor seeds from all l-w lists
        double bestLengthFirstPHit;
        int bestLengthIndex = getBestLengthIndex(bestSeedsForS, sAnalyzer, s, false, bestLengthFirstPHit);
        //cout<<"best length computed\n";
        //SharedFunctions::printElapsedTime(t0);
        t0 = Clock::now();
        map<double, vector<bool>> bestSeedsForSByPHit;
        bestSeedsForSByPHit[bestLengthFirstPHit] = bestSeedsForS[bestLengthIndex].begin()->second;
        //todo avoid p recomputation for the first seed -> done
        map<double, vector<bool> >::iterator it1 = bestSeedsForS[bestLengthIndex].begin();
        it1++;
        for (; it1 != bestSeedsForS[bestLengthIndex].end(); it1++) {
            s.push_back(it1->second);
            sAnalyzer.addSeed(it1->second);
            double pMeasure = sAnalyzer.getProbability(r, p);
            possiblyInsertSeed(it1->second, pMeasure, bestSeedsForSByPHit, true, decreasingBranching);
            s.pop_back();
            sAnalyzer.pop_back(true);
        }
        //cout<<"seeds ordered\n";
        //SharedFunctions::printElapsedTime(t0);
        map<double, vector<bool> >::reverse_iterator it2 = bestSeedsForSByPHit.rbegin();
        for (; it2 != bestSeedsForSByPHit.rend(); it2++) {
            //avoid oc recomputation if possible
            unsigned pMeasure = sAnalyzer.getIncrementalOverlapComplexity(sOC, it2->second);
            s.push_back(it2->second);
            sAnalyzer.addSeed(it2->second);
            recursiveFindBestSeedSetWithOCHeuristic(s, sAnalyzer, depth + 1, pMeasure);
            s.pop_back();
            sAnalyzer.pop_back(true);
        }

    }
    delete [] bestSeedsForS;
}

void BestSetSeeeker::getBestSeedSet() {
    bestSeedComputationStart = Clock::now();
    vector < vector<bool> > s;
    DPAlgorithm sAnalyzer;
    recursiveFindBestSeedSetWithOCHeuristic(s, sAnalyzer, 0, 0);
    SharedFunctions::printSeedSet(bestSeedSet);
    DPAlgorithm seedsAnalyzer;
    seedsAnalyzer.setSeeds(bestSeedSet);
    double pHit = seedsAnalyzer.getProbability(r, p);
    cout << pHit << "\n";

    SharedFunctions::printElapsedTime(bestSeedComputationStart);
}
