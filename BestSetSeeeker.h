#ifndef BESTSETSEEEKER_H
#define	BESTSETSEEEKER_H

#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <map>
#include "SharedFunctions.h"
#include "DPAlgorithm.h"
using namespace std;

class BestSetSeeeker {
public:
    BestSetSeeeker(int l, int w, int n, int r, double p, int bF, int cPL, int mBD);
    void recursiveFindBestSeedSetWithOCHeuristic(vector<vector<bool>>& s, DPAlgorithm & sAnalyzer, int depth, unsigned sOC);
    void getBestSeedSet();
    virtual ~BestSetSeeeker();
private:
    void possiblyInsertSeed(vector<bool> & seed, double pMeasure, map<double, vector < bool>> &bestSeedsForS, bool ascending, int bf);
    int getBestLengthIndex(map<double, vector<bool>>*bestSeedsForS, DPAlgorithm & sAnalyzer, vector<vector<bool> >& s, bool completeSet, double & pHit);
    void possiblyUpdateBestSeedSet(vector<vector<bool>>& s, double pHit);
    int branchingFactor;
    double bestP;
    vector<vector<bool> > bestSeedSet;
    int nSeeds;
    int r; //homologous region length
    double p; //homologous region conservation probability
    int l; //seeds max length
    int w; //seeds weigth
    int candidatesPerLength;
    int maxBranchingDepth;
    Clock::time_point bestSeedComputationStart;

};

#endif	/* BESTSETSEEEKER_H */

