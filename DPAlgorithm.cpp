#include "DPAlgorithm.h"
#include "SharedFunctions.h"
#include <algorithm>
#include <queue>
#include <iostream>

using namespace std;

unsigned DPAlgorithm::uToOnes[1 << 16];

void DPAlgorithm::initLookUpStructures() {
    //init uToOnes
    unsigned size = (1 << 16);
    for (unsigned i = 0; i < size; i++) {
        bitset b(16, i);
        uToOnes[i] = b.count();
    }
}

DPAlgorithm::DPAlgorithm() : seedMaxSize(0) {
}

DPAlgorithm::DPAlgorithm(const DPAlgorithm& orig) {
}

DPAlgorithm::~DPAlgorithm() {
}

void computeProperTails(vector<vector<bitset> > & properTails, vector<bitset> & seeds) {
    for (int i = 0; i < seeds.size(); i++) {
        bitset b;
        for (int j = 0; j < seeds[i].size(); j++) {
            properTails[j].push_back(b);
            b.push_back(seeds[i][j]);
        }
    }

    /*for(int i=0;i<properTails.size();i++) {
        for(int j=0;j<properTails[i].size();j++) {
            cout<<properTails[i][j]<<" ";
        }
        cout<<" "<<i<<"\n";
    }*/
}

void DPAlgorithm::computeSuffixSet() {

    vector<vector<bitset> > properTails(seedMaxSize + 1);
    computeProperTails(properTails, seeds);
    //cout<<"proper tails: ";
    //SharedFunctions::printElapsedTime(t0);
    int count = 0;
    queue<bitset> bQueue;
    bitset epsilon;
    bQueue.push(epsilon);
    suffixSet.push_back(epsilon);
    bitsetToIndex[epsilon] = count++;
    while (!bQueue.empty()) {
        bitset b = bQueue.front();
        bQueue.pop();
        b.push_back(0);
        bool hitsSomeTail = false;
        unsigned bSize = b.size();
        //check wether b is compatible with any tail of the same size
        //cout<<b<<"\n";
        for (int i = 0; i < properTails[bSize].size() && !hitsSomeTail; i++) {
            hitsSomeTail = properTails[bSize][i].is_subset_of(b);
        }
        if (hitsSomeTail) {
            bQueue.push(b);
            suffixSet.push_back(b);
            bitsetToIndex[b] = count++;
        }

        b[b.size() - 1] = 1;
        //adding
        bool hitByA = false;
        for (int i = 0; i < seeds.size() && !hitByA; i++) {
            int delta = b.size() - seeds[i].size();
            if (delta >= 0) {
                bitset extendedSeed = seeds[i];
                for (int d = 0; d < delta; d++) {
                    extendedSeed.push_back(0);
                }
                for (int d = 0; d <= delta; d++) {
                    if (extendedSeed.is_subset_of(b)) {
                        hitByA = true;
                        break;
                    }
                    extendedSeed <<= 1;
                }
            }

        }
        if (!hitByA) {
            bQueue.push(b);
            suffixSet.push_back(b);
            bitsetToIndex[b] = count++;
        }

    }
    suffixProperties.reserve(suffixSet.size());
    for (int i = 0; i < suffixSet.size(); i++) {
        Suffix suffix;
        bitset oneB = suffixSet[i];
        oneB.push_back(1);
        map<bitset, int>::iterator it = bitsetToIndex.find(oneB);
        if (it != bitsetToIndex.end()) {
            suffix.oneB = it->second;
        } else {
            suffix.oneB = -1;
        }
        bitset bPrime = suffixSet[i];
        int j = 0;
        bPrime.push_back(0);
        int startSize = bPrime.size();
        it = bitsetToIndex.find(bPrime);
        while (it == bitsetToIndex.end()) {
            j++;
            bPrime >>= 1;
            bPrime.resize(bPrime.size() - 1);
            it = bitsetToIndex.find(bPrime);
        }
        suffix.bPrime = it->second;
        suffix.j = startSize - bPrime.size();
        suffixProperties.push_back(suffix);
    }
}

double DPAlgorithm::getProbability(unsigned regionLength, double p) {
    computeSuffixSet();
    double q = 1 - p;
    unsigned ssSize = suffixSet.size();
    double ** f = new double*[regionLength];
    for (int i = 0; i < regionLength; i++) {
        f[i] = new double[ssSize];
    }
    for (int i = 0; i < regionLength; i++) {
        for (int j = ssSize - 1; j >= 0; j--) {
            if (i < suffixSet[j].size()) {
                f[i][j] = 0;
            } else {
                //cout<<suffixSet[j]<<" "<<suffixSet[suffixProperties[j].bPrime]<<" "<<suffixProperties[j].j;
                int delta = suffixProperties[j].j;
                double f0 = 0;
                if (i - delta > 0) {
                    f0 = f[i - delta][suffixProperties[j].bPrime];
                }
                double f1 = 1;
                if (!(suffixProperties[j].oneB == -1)) {
                    f1 = f[i][suffixProperties[j].oneB];
                }
                f[i][j] = q * f0 + p*f1;
            }
        }
    }

    double result = f[regionLength - 1][0];
    for (int i = 0; i < regionLength; i++) {
        delete [] f[i];
    }
    delete [] f;
    return result;

}

unsigned DPAlgorithm::countOnes(unsigned long b) {
    unsigned res = 0;
    //here we are assuming that seeds are no longer than 32
    for (int i = 0; i < 2; i++) {
        res += uToOnes[b & 65535];
        b >>= 16;
    }
    return res;
}

unsigned DPAlgorithm::getOverlapComplexity2Seeds2(bitset b1, bitset b2) {
    unsigned b2Size = b2.size();
    unsigned b1Size = b1.size();
    unsigned oc = 4;
    unsigned long ub1 = b1.to_ulong();
    unsigned long ub2 = b2.to_ulong();
    //as they are
    oc += (1 << countOnes(ub2 & ub1));
    //b2 shifting right
    for (unsigned i = 1; i <= b2Size - 2; i++) {
        oc += (1 << countOnes(ub2 >> i & ub1));
    }
    //b1 shifting right
    for (unsigned i = 1; i <= b1Size - 2; i++) {
        oc += (1 << countOnes(ub1 >> i & ub2));
    }
    return oc;
}

unsigned DPAlgorithm::getOverlapComplexity() {

    int nSeeds = seeds.size();
    unsigned oc = 0;
    for (int i = 0; i < nSeeds; i++) {
        for (int j = i; j < nSeeds; j++) {
            oc += getOverlapComplexity2Seeds2(seeds[i], seeds[j]);
        }
    }
    return oc;
}

unsigned DPAlgorithm::getIncrementalOverlapComplexity(unsigned sOC, vector<bool> & seed) {
    bitset b;
    for (int j = 0; j < seed.size(); j++) {
        b.push_back(seed[j]);
    }
    return getIncrementalOverlapComplexity(sOC, b);
}

unsigned DPAlgorithm::getIncrementalOverlapComplexity(unsigned sOC, bitset seed) {
    sOC += getOverlapComplexity2Seeds2(seed, seed);
    for (int i = 0; i < seeds.size(); i++) {
        sOC += getOverlapComplexity2Seeds2(seeds[i], seed);
    }
    return sOC;
}

void DPAlgorithm::readAndCompute() {
    cin >> *this;
    Clock::time_point t0 = Clock::now();
    cout << getProbability(64, 0.7) << endl;
    cout << getOverlapComplexity() << "\n";
    SharedFunctions::printElapsedTime(t0);
}

bool bitsetComparator(const bitset & b1, const bitset & b2) {
    if (b1.size() != b2.size()) {
        return b1.size() > b2.size();
    }
    return b1>b2;
}

istream &operator>>(istream &input, DPAlgorithm &s) {
    int nSeeds;
    input>>nSeeds;
    for (int i = 0; i < nSeeds; i++) {
        int seedDim;
        input>>seedDim;
        bitset seed(seedDim, 0);
        for (int j = 0; j < seedDim; j++) {
            int v;
            input >> v;
            seed[seedDim - 1 - j] = v;
        }
        s.seeds.push_back(seed);
        if (seedDim > s.seedMaxSize) {
            s.seedMaxSize = seedDim;
        }
    }
    //sort(s.seeds.begin(), s.seeds.end(), bitsetComparator);
    /*for(int i=0; i<s.seeds.size(); i++) {
        cout<<s.seeds[i]<<",";
    }
    cout<<"\n";
     * */
    return input;
}

void DPAlgorithm::pop_back(bool clearDS) {
    pop_back();
    suffixSet.clear();
    bitsetToIndex.clear();
    suffixProperties.clear();
}

void DPAlgorithm::pop_back() {
    bool longest = (seeds.rbegin()->size() == seedMaxSize);
    seeds.pop_back();
    
    if(!longest) {
        return;
    }
    seedMaxSize = 0;
    for (int j = 0; j < seeds.size(); j++) {
        if(seeds[j].size() > seedMaxSize) {
            seedMaxSize = seeds[j].size();
        }
    }
}


void DPAlgorithm::addSeed(vector<bool>& seed) {
    bitset b;
    for (int j = 0; j < seed.size(); j++) {
        b.push_back(seed[j]);
    }
    seeds.push_back(b);
    if (seed.size() > seedMaxSize) {
        seedMaxSize = seed.size();
    }
}

void DPAlgorithm::setSeeds(vector<vector<bool>> &seedSet) {
    seeds.clear();
    seeds.reserve(seedSet.size());
    for (int i = 0; i < seedSet.size(); i++) {
        bitset b;
        for (int j = 0; j < seedSet[i].size(); j++) {
            b.push_back(seedSet[i][j]);
        }
        //b.append(seedSet[i].begin(), seedSet[i].end());
        seeds.push_back(b);
        if (b.size() > seedMaxSize) {
            seedMaxSize = b.size();
        }
        //cout << b << "\n";
    }

}