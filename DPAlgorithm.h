#ifndef DPALGORITHM_H
#define	DPALGORITHM_H

#include "boost/dynamic_bitset.hpp"
#include "AbstractSeedsAnalyzer.h"
#include "unordered_map"
#include <map>

typedef boost::dynamic_bitset<> bitset;

using namespace std;

struct CompareBitsets {
    bool operator()(const bitset & b1, const bitset & b2) const {
        if(b1.size() != b2.size()) {
            return b1.size()<b2.size();
        }
        return b1<b2;
    }
};

struct bitsetEquals {

    bool operator()(bitset const& s1, bitset const& s2) const {
        if(s1.size() != s2.size()) {
            return false;
        }
        return s1==s2;
    }
};

struct bitsetHasher {

    ulong operator()(bitset const& s) const {
        bitset copy = s;
        copy.push_back(1);
        return copy.to_ulong()-1;
    }
};

class Suffix {
public:
    unsigned j;
    unsigned bPrime;
    int oneB;
};
class DPAlgorithm : public AbstractSeedsAnalyzer {
public:
    DPAlgorithm();
    DPAlgorithm(const DPAlgorithm& orig);
    virtual ~DPAlgorithm();
    void computeSuffixSet();
    unsigned getOverlapComplexity();
    unsigned getIncrementalOverlapComplexity(unsigned sOC, bitset seed);
    unsigned getIncrementalOverlapComplexity(unsigned sOC, vector<bool> & seed);
    virtual double getProbability(unsigned regionLenght, double p);
    unsigned getOverlapComplexity2Seeds2(bitset b1, bitset b2);
    virtual void readAndCompute();
    friend istream &operator>>(istream &input, DPAlgorithm &s);
    void setSeeds(vector<vector<bool>> & seeds);
    void addSeed(vector<bool> & seed);
    void pop_back();
    void pop_back(bool clearDS);
    static void initLookUpStructures();
    
private:
    unsigned countOnes(unsigned long b);
    vector<bitset> suffixSet;
    map<bitset, int, CompareBitsets> bitsetToIndex;
    vector<Suffix> suffixProperties;
    vector<bitset> seeds;
    int seedMaxSize=0;
    static unsigned uToOnes[1<<16];
};

#endif	/* DPALGORITHM_H */

