#include "RandomSeedsGenerator.h"
#include "DPAlgorithm.h"
#include <cstdlib>
#include <set>
#include <iostream>
using namespace std;

RandomSeedsGenerator::RandomSeedsGenerator() {
}

RandomSeedsGenerator::RandomSeedsGenerator(const RandomSeedsGenerator& orig) {
}

RandomSeedsGenerator::~RandomSeedsGenerator() {
}

vector<vector<bool> > RandomSeedsGenerator::getRandomSeeds(int w, int l, int n) {
    vector<vector<bool>> randomSeeds;
    for(int i=0;i<n;i++) {
        randomSeeds.push_back(getRandomSeed(w,l));
    }
    return randomSeeds;
}

vector<bool> RandomSeedsGenerator::getRandomSeed(int w, int l) {
    vector<bool> randomSeed(l, true);
    set<int> positions;
    for(int i=1;i<l-1;i++) {
        positions.insert(i);
    }
    for(int i=0;i<l-w;i++) {
        int p = rand()%positions.size();
        set<int>::const_iterator it(positions.begin());
        advance(it, p);
        randomSeed[*it]=false;
        positions.erase(it);
    }
    return randomSeed;
}

void RandomSeedsGenerator::computeRandomSetsSensitivityAndOC(int nSets, int nSeeds) {
    int cont1 = 0;
    int cont2 = 0;
    for(int i=0;i<nSets;i++) {
        vector<vector<bool> >randomSet = getRandomSeeds(9,15, nSeeds);
        vector<vector<bool> >randomSet2 = getRandomSeeds(9,15, nSeeds);
        DPAlgorithm a;
        DPAlgorithm a2;
        a.setSeeds(randomSet);
        a2.setSeeds(randomSet2);
        double p1 = a.getProbability(64,0.7);
        double p2 = a2.getProbability(64,0.7);
        int oc1 = a.getOverlapComplexity();
        int oc2 = a2.getOverlapComplexity();
        //cout<<oc1<<" "<<oc2<<" "<<p1<<" "<<p2<<"\n";
        if(oc1 > oc2 && p1 < p2 || oc2 > oc1 && p2 < p1) {
            cont1++;
        }
        else if(oc1!=oc2 && p1!=p2){
           cont2++;
        }
    }
    cout<<cont1<<" common cases\n";
    cout<<cont2<<" uncommon cases\n";
}


