#ifndef RANDOMSEEDSGENERATOR_H
#define	RANDOMSEEDSGENERATOR_H

#include <vector>
using namespace std;

class RandomSeedsGenerator {
public:
    RandomSeedsGenerator();
    RandomSeedsGenerator(const RandomSeedsGenerator& orig);
    virtual ~RandomSeedsGenerator();
    vector< vector<bool> > getRandomSeeds(int w, int l, int n);
    vector<bool> getRandomSeed(int w, int l);
    void computeRandomSetsSensitivityAndOC(int nSets, int nSeeds);
private:

};

#endif	/* RANDOMSEEDSGENERATOR_H */

