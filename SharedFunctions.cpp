#include "SharedFunctions.h"
#include <iostream>

SharedFunctions::SharedFunctions() {
}

SharedFunctions::SharedFunctions(const SharedFunctions& orig) {
}

SharedFunctions::~SharedFunctions() {
}

void SharedFunctions::printElapsedTime(Clock::time_point t0) {
    Clock::time_point t1 = Clock::now();
    auto ms = chrono::duration_cast<chrono::milliseconds>(t1 - t0);
    cout << ms.count()/1000.0f << " s\n";
}

void SharedFunctions::printSeedSet(vector<vector<bool> >& s) {
    for (int i = 0; i < s.size(); i++) {
        for (int j = 0; j < s[i].size(); j++) {
            cout << s[i][j] << " ";
        }
        cout << endl;
    }
}