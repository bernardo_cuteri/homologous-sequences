#include <ctime>
#include <time.h>
#include <chrono>
#include <vector>
using namespace std;

#ifndef SHAREDFUNCTIONS_H
#define	SHAREDFUNCTIONS_H
typedef chrono::high_resolution_clock Clock;

class SharedFunctions {
public:
    SharedFunctions();
    SharedFunctions(const SharedFunctions& orig);
    virtual ~SharedFunctions();
    static void printElapsedTime(Clock::time_point t0);
    static void printSeedSet(vector<vector<bool> >& s);

private:

};

#endif	/* SHAREDFUNCTIONS_H */

