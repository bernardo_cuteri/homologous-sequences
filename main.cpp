#include<iostream>
#include "BestSetSeeeker.h"
#include "DPAlgorithm.h"
#include "boost/dynamic_bitset.hpp"
#include "RandomSeedsGenerator.h"
#include "SharedFunctions.h"
#include <boost/program_options.hpp>
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>


namespace po = boost::program_options;
using namespace std;

void initialize(vector<bool> & seed, int li, int w, bool flag) {
    seed[0] = seed[li - 1] = 1;
    for (int i = 0; i < w - 2; i++) {
        seed[li - i - 2] = 1;
    }
}


void testOCComputationTime(int l, int w) {
    Clock::time_point t0 = Clock::now();
    vector<bool> seed(l, 0);
    initialize(seed, l, w, true);
    int cont = 0;
    DPAlgorithm seedsAnalyzer;
    do {
        bitset b(l,0);
        for(int i=0;i<l;i++) {
            b[i]=seed[i];
        }
        //avoid to compute p on a seed and the symmetric one (we know it's the same)
        //assert(seedsAnalyzer.getOverlapComplexity2Seeds2(b, b) == seedsAnalyzer.getOverlapComplexity2Seeds(b, b));
        seedsAnalyzer.getOverlapComplexity2Seeds2(b, b);
        cont++;
    } while (next_permutation(seed.begin() + 1, seed.end() - 1));
    cout<<l<<" "<<cont<<" ";
    SharedFunctions::printElapsedTime(t0);
    
}


int main(int argc, char** argv) {

    //initialize look-up table to count 1's in binary strings
    DPAlgorithm::initLookUpStructures();
    
    //dummy code for benchmarks
    /*for(int w=17,l=25;l<=30;l++,w++) {
        testOCComputationTime(l, w);        
    }*/
    //RandomSeedsGenerator g;
    //g.computeRandomSetsSensitivityAndOC(1000,4);
    
    //DPAlgorithm analyzer;
    //analyzer.readAndCompute();
    
    //problem specification parameters
    int l; //seeds max length
    int w; //seeds weight
    int n; //number of seeds
    int r; //homologous region length
    double p; //conservation probability
    
    //search related parameters
    int branching;
    int candidatesPerLength;
    int maxBranchingDepth;
    
    po::options_description desc("Allowed options");
    desc.add_options()
    ("help", "produce help message")
    ("l", po::value<int>(&l)->default_value(28), "seeds max length")
    ("w", po::value<int>(&w)->default_value(11), "seeds weight")
    ("n", po::value<int>(&n)->default_value(16), "multiple seed cardinality")
    ("r", po::value<int>(&r)->default_value(64), "homologous region length")
    ("p", po::value<double>(&p)->default_value(0.7), "conservation probability")
    ("r", po::value<int>(&branching)->default_value(4), "branching factor")
    ("r", po::value<int>(&candidatesPerLength)->default_value(4), "candidates per length")
    ("r", po::value<int>(&maxBranchingDepth)->default_value(1), "max branching depth")
    ;
    
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
    
    if (vm.count("help")) {
        cout << desc << "\n";
        return 1;
    }
    
    BestSetSeeeker b(l, w, n, r, p, branching, candidatesPerLength, maxBranchingDepth);
    b.getBestSeedSet();
    
    
    return 0;
}

