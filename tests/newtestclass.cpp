#include <ctime>
#include <time.h>
#include <chrono>
#include <algorithm>
#include "newtestclass.h"
#include "../DPAlgorithm.h"
#include "../SharedFunctions.h"

using namespace std;

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::milliseconds milliseconds;

CPPUNIT_TEST_SUITE_REGISTRATION(newtestclass);

newtestclass::newtestclass() {
}

newtestclass::~newtestclass() {
}

void newtestclass::setUp() {
}

void newtestclass::tearDown() {
}

void submitFromFile(string fileName, double expectedP) {
    Clock::time_point t0 = Clock::now();
    DPAlgorithm s;
    ifstream inFile(fileName);
    if (inFile.is_open()) {
        
        inFile>>s;
        inFile.close();
    } else {
        cout << "\nUnable to open file\n"<<fileName<<endl;
        CPPUNIT_ASSERT(false);
    }

    double p = s.getProbability(64, 0.7);
    double delta = 0.000001;
    CPPUNIT_ASSERT(p >= expectedP - delta);
    CPPUNIT_ASSERT(p <= expectedP + delta);
    cout<<"\n";
    SharedFunctions::printElapsedTime(t0);
}

void newtestclass::testOptimalSingleSeedWeight11() {
    submitFromFile("testcases/optimalWeight11.dat", 0.467122);
}


void newtestclass::testSubSeed() {
    
    double delta = 0.000001;
    
    vector<bool> seed;
    seed.push_back(1);
    seed.push_back(1);
    seed.push_back(0);
    seed.push_back(1);
    seed.push_back(1);
    seed.push_back(1);
    seed.push_back(1);
    
    vector<bool> seed2;
    seed2.push_back(1);
    seed2.push_back(1);
    seed2.push_back(0);
    seed2.push_back(1);
    seed2.push_back(1);
    DPAlgorithm s1;
    s1.addSeed(seed2);
    double p1 = s1.getProbability(64, 0.7);
    cout<<p1<<endl;
    DPAlgorithm s2;
    s2.addSeed(seed);
    s2.addSeed(seed2);
    double p2 = s2.getProbability(64, 0.7);
    cout<<p2<<endl;
    CPPUNIT_ASSERT(p1 > p2-delta);
    CPPUNIT_ASSERT(p1 < p2+delta);
    
    
}

void newtestclass::testSymmetry() {
    double delta = 0.000001;
    
    vector<bool> seed;
    seed.push_back(1);
    seed.push_back(0);
    seed.push_back(1);
    seed.push_back(1);
    seed.push_back(1);
    seed.push_back(1);
    
    DPAlgorithm s;
    s.addSeed(seed);
    double p = s.getProbability(64, 0.7);
    
    DPAlgorithm s2;
    //s2.addSeed(seed, seed.size());
    reverse(seed.begin(), seed.end());
    s2.addSeed(seed);
    double p2 = s2.getProbability(64, 0.7);
    CPPUNIT_ASSERT(p > p2-delta);
    CPPUNIT_ASSERT(p < p2+delta);
    
    
}