/*
 * File:   newtestclass.h
 * Author: bernardo
 *
 * Created on Aug 9, 2015, 10:28:16 AM
 */

#ifndef NEWTESTCLASS_H
#define	NEWTESTCLASS_H

#include <cppunit/extensions/HelperMacros.h>

class newtestclass : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(newtestclass);

    CPPUNIT_TEST(testOptimalSingleSeedWeight11);
    
    CPPUNIT_TEST(testSymmetry);
    
    CPPUNIT_TEST(testSubSeed);

    CPPUNIT_TEST_SUITE_END();

public:
    newtestclass();
    virtual ~newtestclass();
    void setUp();
    void tearDown();

private:
    void testOptimalSingleSeedWeight11();
    void testSubSeed();
    void testSymmetry();
};

#endif	/* NEWTESTCLASS_H */

